package pl.poznan.put.spio;

// To jest miejsce od którego należy zacząć zadanie TDD

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class StackTddTest {

    private Stack stack;

    @BeforeEach
    public void init() {
        stack = new Stack();
        stack.push("aaa");
        stack.push("bbb");
    }

    @Test
    void shouldReturnStackCopy() {
        final Collection<Object> stackElements = Arrays.asList("aaa", "bbb");

        final Collection<Object> stackCopy = stack.getStackCopy();

        assertArrayEquals(stackElements.toArray(), stackCopy.toArray());
    }

    @Test
    void shouldReturnReversedStack() {
        final Collection<Object> stackElements = Arrays.asList("bbb", "aaa");

        final Collection<Object> reversedStack = stack.getReversedStack();

        assertArrayEquals(stackElements.toArray(), reversedStack.toArray());
    }
}