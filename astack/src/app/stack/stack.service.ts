// klauzula import pozwala korzystać z zewnętrznych modułów i klas, tutaj z dekoratora Injectable.
import { Injectable } from '@angular/core';

/* Klasa opisująca stos, dodatkowo oznaczona dekoratorem @Injectable()
* angulara, co oznacza że jest to serwis wstrzykiwalny w innych miejscach systemu
*/
@Injectable()
export class Stack {

    // tablica, w której trzymamy elementy stosu
    private _stack = [];

    // metoda push przyjmuje element dowolnego typu i umieszcza go w tablicy
    push(element: any) {
        this._stack.push(element);
    }

    // metoda pop zdejmuje wierzchni element ze stosu
    pop(): any {
        return this._stack.pop();
    }

    // metoda size pobiera i zwraca bieżącą wielkość stosu
    size(): number {
        return this._stack.length;
    }

    // metoda elements zwraca kompletny stos
    elements(): any[] {
        return this._stack;
    }

}
